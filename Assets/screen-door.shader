Shader "Screen-door"
{
	Properties 
	{
_MainTex("Base (RGB) Gloss (A)", 2D) = "gray" {}
_Color("Main Color", Color) = (1,1,1,0.5019608)
_Opacity("_Opacity", Float) = 0

	}
	
	SubShader 
	{
		Tags
		{
"Queue"="Geometry"
"IgnoreProjector"="False"
"RenderType"="Opaque"

		}

		
Cull Off
ZWrite On
ZTest LEqual
ColorMask RGBA
Fog{
}


		CGPROGRAM
#pragma surface surf BlinnPhongEditor  vertex:vert
#pragma target 2.0


sampler2D _MainTex;
float4 _Color;
float _Opacity;

			struct EditorSurfaceOutput {
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half3 Gloss;
				half Specular;
				half Alpha;
				half4 Custom;
			};
			
			inline half4 LightingBlinnPhongEditor_PrePass (EditorSurfaceOutput s, half4 light)
			{
half3 spec = light.a * s.Gloss;
half4 c;
c.rgb = (s.Albedo * light.rgb + light.rgb * spec);
c.a = s.Alpha;
return c;

			}

			inline half4 LightingBlinnPhongEditor (EditorSurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
			{
				half3 h = normalize (lightDir + viewDir);
				
				half diff = max (0, dot ( lightDir, s.Normal ));
				
				float nh = max (0, dot (s.Normal, h));
				float spec = pow (nh, s.Specular*128.0);
				
				half4 res;
				res.rgb = _LightColor0.rgb * diff;
				res.w = spec * Luminance (_LightColor0.rgb);
				res *= atten * 2.0;

				return LightingBlinnPhongEditor_PrePass( s, res );
			}
			
			struct Input {
				float4 color : COLOR;
float2 uv_MainTex;
float4 screenPos;

			};

			void vert (inout appdata_full v, out Input o) {
v.vertex = v.vertex;
v.color = v.color;
v.normal = (float4( v.normal.x, v.normal.y, v.normal.z, 1.0 )).xyz;
v.tangent = float4( 0.0, 0.0, 0.0, 0.0 );


			}
			

			void surf (Input IN, inout EditorSurfaceOutput o) {
				o.Normal = float3(0.0,0.0,1.0);
				o.Alpha = 1.0;
				o.Albedo = 0.0;
				o.Emission = 0.0;
				o.Gloss = 0.0;
				o.Specular = 0.0;
				o.Custom = 0.0;
				
float4 SplatAlpha1=IN.color.w;
float4 Add2=IN.color + SplatAlpha1;
float4 Sampled2D0=tex2D(_MainTex,IN.uv_MainTex.xy);
float4 Multiply0=Add2 * Sampled2D0;
float4 Multiply2=_Color * Multiply0;
float4 Multiply4=Sampled2D0.aaaa * _Opacity.xxxx;
float4 SplatAlpha0=_Color.w;
float4 Split0=((IN.screenPos.xy/IN.screenPos.w).xyxy);
float4 Add3_1_NoInput = float4(0,0,0,0);
float4 Add3=float4( Split0.w, Split0.w, Split0.w, Split0.w) + Add3_1_NoInput;
float4 Add1=float4( Split0.x, Split0.x, Split0.x, Split0.x) + Add3;
float4 Split2=IN.screenPos;
float4 Multiply3=float4( Split2.z, Split2.z, Split2.z, Split2.z) * float4( 0.001,0.001,0.001,0.001 );
float4 Add5=Add1 + Multiply3;
float4 Multiply1=Add5 * float4( 200,200,200,200 );
float4 Frac0=frac(Multiply1);
float4 Subtract0=SplatAlpha0 - Frac0;
float4 Add0=Multiply4 + Subtract0;
float4 Master0_1_NoInput = float4(0,0,1,1);
float4 Master0_2_NoInput = float4(0,0,0,0);
float4 Master0_3_NoInput = float4(0,0,0,0);
float4 Master0_4_NoInput = float4(0,0,0,0);
float4 Master0_5_NoInput = float4(1,1,1,1);
float4 Master0_7_NoInput = float4(0,0,0,0);
clip( Add0 );
o.Albedo = Multiply2;

				o.Normal = normalize(o.Normal);
			}
		ENDCG
	}
	Fallback "Diffuse"
}