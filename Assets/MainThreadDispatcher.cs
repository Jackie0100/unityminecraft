﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Threading;

public class MainThreadDispatcher : MonoBehaviour
{
    static MainThreadDispatcher EventHandler;
    static Queue<Action> EventQueue = new Queue<Action>();

    private MainThreadDispatcher()
    {
        EventHandler = this;
    }

    void Update()
    {
        Action act;
        while (EventQueue.Count != 0)
        {
            lock (EventQueue)
            {
                act = EventQueue.Dequeue();
            }
            act.Invoke();
        }
    }

    public static void RunOnMainThread(Action action)
    {
        lock (EventQueue)
        {
            EventQueue.Enqueue(action);
        }
    }
}
