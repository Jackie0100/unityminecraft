using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(CharacterMotor))]

public class MMOController : MonoBehaviour
{
    private GameObject character;
    private CharacterMotor motor;
    private MMOCameraController mmocamera;


    public void Awake()
    {
        motor = this.GetComponent<CharacterMotor>();
        mmocamera = GameObject.Find("Main Camera").GetComponent<MMOCameraController>();
        character = this.gameObject;
    }

    void Update()
    {
        Vector3 dir = new Vector3();
        if (Input.GetKey(KeyCode.W))
        {
            dir += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            dir += Vector3.back;
        }
        if (Input.GetKey(KeyCode.A))
        {
            dir += Vector3.left;
        }
        if (Input.GetKey(KeyCode.D))
        {
            dir += Vector3.right;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 20))
            {
                Debug.Log(hit.transform);
                Chunk chunk = hit.transform.GetComponent<Chunk>();
                if (chunk != null)
                {
                    Vector3 p = hit.point + (0.0001f * ray.direction);
                    p += (hit.normal * -0.5f);

                    chunk.SetBlock(0, p);
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 20))
            {
                Debug.Log(hit.transform);
                Chunk chunk = hit.transform.GetComponent<Chunk>();
                if (chunk != null)
                {
                    Vector3 p = hit.point - (0.0001f * ray.direction);
                    p -= (hit.normal * -0.5f);
                    
                    chunk.SetBlock(1, p);
                }
            }
        }
        motor.inputMoveDirection = transform.TransformDirection(dir) * 3;
        motor.inputJump = Input.GetKey(KeyCode.Space);
    }
}