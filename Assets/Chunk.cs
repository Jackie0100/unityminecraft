﻿using LibNoise;
using LibNoise.Generator;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class Chunk : MonoBehaviour
{
    static List<Chunk> ChunkQueue = new List<Chunk>();
    private static bool isgenerating;
    private byte[, ,] Map;
    private uint[, ,] LightMap;

    Block GrassBlock = new Block(1, 1, 4, 3);
    Block SnowBlock = new Block(2, 67, 69, 3);
    Block SandBlock = new Block(3, 19, 19, 19);

    private Block[] Blocks;

    Mesh Mesh { get; set; }
    // Use this for initialization
    void Start()
    {
        Blocks = new[] { GrassBlock, GrassBlock, SnowBlock, SandBlock };
        ChunkQueue.Add(this);
        ChunkQueue = ChunkQueue.OrderBy(c => Vector3.Distance(c.transform.position + new Vector3(8, 0, 8), Camera.main.transform.position)).ToList<Chunk>();
    }

    void OnDestroy()
    {
        ChunkQueue.Remove(this);

        //TODO: Save Changes
    }

    //void ChangeColor()
    //{
    //    List<Color> c = new List<Color>();
    //    for (int i = 0; i < Mesh.colors.Length; i++)
    //    {
    //        //Color col = Mesh.colors[i];
    //        c.Add(new Color(1, 0, 0, 1));
    //    }
    //    Mesh.colors = c.ToArray();
    //}

    // Update is called once per frame
    void Update()
    {
        if (ChunkQueue.Count > 0 && !isgenerating)
        {
            isgenerating = true;
            Chunk chunk;

            chunk = ChunkQueue[0];
            ChunkQueue.RemoveAt(0);
            chunk.CreateMap();
            chunk.CreateLightMap();
            StartCoroutine(chunk.CreateMesh());
            chunk.GetComponent<MeshRenderer>().material = World.SpriteSheet;
        }
    }

    void CreateMap()
    {
        Map = new byte[World.ChunkSize, World.ChunkHeight, World.ChunkSize];

        Perlin noise = new Perlin(0.5, 2, 0.5, 4, World.CurrentWorld.Seed, QualityMode.High);
        Perlin biomes = new Perlin(0.1, 1, 1, 1, World.CurrentWorld.Seed, QualityMode.High);

        for (int x = 0; x < World.ChunkSize; x++)
        {
            for (int y = 0; y < World.ChunkHeight; y++)
            {
                for (int z = 0; z < World.ChunkSize; z++)
                {
                    double noiseval = noise.GetValue(
                        (this.transform.position.x / (double)World.ChunkSize) + ((double)x / (double)World.ChunkSize),
                        (double)y / (double)World.ChunkHeight,
                        (this.transform.position.z / (double)World.ChunkSize) + ((double)z / (double)World.ChunkSize));
                    double biomeval = (biomes.GetValue(
                        (this.transform.position.x/(double) World.ChunkSize) + ((double) x/(double) World.ChunkSize),
                        0,
                        (this.transform.position.z/(double) World.ChunkSize) + ((double) z/(double) World.ChunkSize)) * 0.5) + 0.5;
                    // (height - y) / difference
                    noiseval += (20.0 - y) / 10.0;
                    if (noiseval > 0)
                    {
                        if (biomeval * 3 < 1)
                        {
                            Map[x, y, z] = 1;
                        }
                        else if (biomeval * 3 < 2)
                        {
                            Map[x, y, z] = 2;
                        }
                        else
                        {
                            Map[x, y, z] = 3;
                        }
                    }
                }
            }
        }
    }

    public void CreateLightMap()
    {
        LightMap = new uint[World.ChunkSize, World.ChunkHeight, World.ChunkSize];
        for (int x = 0; x < World.ChunkSize; x++)
        {
            for (int y = 0; y < World.ChunkHeight; y++)
            {
                for (int z = 0; z < World.ChunkSize; z++)
                {
                    if (x < 8)
                        LightMap[x, y, z] = 0x0000088;
                    else
                        LightMap[x, y, z] = 0xffffffff;

                }
            }
        }
    }

    IEnumerator CreateMesh()
    {
        
        List<Vector3> verts = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<int> tris = new List<int>();
        List<Color32> colors = new List<Color32>();

        for (int x = 0; x < World.ChunkSize; x++)
        {
            for (int y = 0; y < World.ChunkHeight; y++)
            {
                for (int z = 0; z < World.ChunkSize; z++)
                {
                    if (Map[x, y, z] == 0)
                        continue;

                    
                    //Left
                    if (IsVissible(x - 1, y, z))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x, y, z), Vector3.up, Vector3.forward, false, verts, uvs, tris, colors, Direction.Left);
                    }
                    //Right
                    if (IsVissible(x + 1, y, z))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x + 1, y, z), Vector3.up, Vector3.forward, true, verts, uvs, tris, colors, Direction.Right);
                    }
                    //Bottom
                    if (IsVissible(x, y - 1, z))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x, y, z), Vector3.forward, Vector3.right, false, verts, uvs, tris, colors, Direction.Bottom);
                    }
                    //Top
                    if (IsVissible(x, y + 1, z))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x, y + 1, z), Vector3.forward, Vector3.right, true, verts, uvs, tris, colors, Direction.Top);
                    }
                    //Back
                    if (IsVissible(x, y, z - 1))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x, y, z), Vector3.up, Vector3.right, true, verts, uvs, tris, colors, Direction.Back);
                    }
                    //Front
                    if (IsVissible(x, y, z + 1))
                    {
                        BuildFaces(Map[x, y, z], LightMap[x, y, z], new Vector3(x, y, z + 1), Vector3.up, Vector3.right, false, verts, uvs, tris, colors, Direction.Front);
                    }

                }
            }
            //yield return new WaitForEndOfFrame();
        }

        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        Mesh = new Mesh();
        Mesh.vertices = verts.ToArray();
        Mesh.uv = uvs.ToArray();
        Mesh.triangles = tris.ToArray();
        Mesh.colors32 = colors.ToArray();
        UnityEngine.Debug.Log(sw.Elapsed);
        yield return new WaitForEndOfFrame();

        Mesh.RecalculateBounds();
        //yield return new WaitForEndOfFrame();

        Mesh.RecalculateNormals();
        Mesh.Optimize();
        //yield return new WaitForEndOfFrame();
        UnityEngine.Debug.Log(sw.Elapsed);
        this.GetComponent<MeshFilter>().mesh = Mesh;
        yield return new WaitForEndOfFrame();
        this.GetComponent<MeshCollider>().sharedMesh = null;
        this.GetComponent<MeshCollider>().sharedMesh = Mesh;
        //yield return new WaitForEndOfFrame();
        sw.Stop();

        UnityEngine.Debug.Log(sw.Elapsed);
        isgenerating = false;
        yield return 0;
    }

    //public static Mesh ReduceMesh(Chunk chunk)
    //{
    //    List<Vector3>  vertices = new List<Vector3>();
    //    List elements = new List();
    //    List<Vector2> uvs = new List<Vector2>();
    //    List colours = new List();
 
    //    List noCollision = World.noCollision;
 
    //    int size = World.CHUNK_SIZE;
 
    //    //Sweep over 3-axes
    //    for (int d = 0; d < 3; d++)
    //    {
 
    //        int i, j, k, l, w, h, u = (d + 1) % 3, v = (d + 2) % 3;
 
    //        int[] x = new int[3];
    //        int[] q = new int[3];
    //        int[] mask = new int[(size + 1) * (size + 1)];
 
    //        q[d] = 1;
 
    //        for (x[d] = -1; x[d] < size; )
    //        {
 
    //            // Compute the mask
    //            int n = 0;
    //            for (x[v] = 0; x[v] < size; ++x[v])
    //            {
    //                for (x[u] = 0; x[u] < size; ++x[u], ++n)
    //                {


    //                    int a = 0;
    //                    if (0 <= x[d])
    //                    {
    //                        a = (int) World.GetBlock(chunk, x[0], x[1], x[2]).Type;
    //                        if (noCollision.IndexOf(a) != -1)
    //                        {
    //                            a = 0;
    //                        }
    //                    }
    //                    int b = 0;
    //                    if (x[d] < size - 1)
    //                    {
    //                        b = (int) World.GetBlock(chunk, x[0] + q[0], x[1] + q[1], x[2] + q[2]).Type;
    //                        if (noCollision.IndexOf(b) != -1)
    //                        {
    //                            b = 0;
    //                        }
    //                    }
    //                    if (a != -1 && b != -1 && a == b)
    //                    {
    //                        mask[n] = 0;
    //                    }
    //                    else if (a > 0)
    //                    {
    //                        a = 1;
    //                        mask[n] = a;
    //                    }
    //                    else
    //                    {
    //                        b = 1;
    //                        mask[n] = -b;
    //                    }
    //                }
    //            }
 
    //            // Increment x[d]
    //            ++x[d];
 
    //            // Generate mesh for mask using lexicographic ordering
    //            n = 0;
    //            for (j = 0; j < size; ++j)
    //            {
    //                for (i = 0; i < size; )                     
    //                {                         
    //                    var c = mask[n];                         
    //                    if (c > -3)
    //                    {
    //                        // Compute width
    //                        for (w = 1; c == mask[n + w] && i + w < size; ++w) { }
 
    //                        // Compute height
    //                        bool done = false;
    //                        for (h = 1; j + h < size; ++h)
    //                        {
    //                            for (k = 0; k < w; ++k)
    //                            {
    //                                if (c != mask[n + k + h*size])
    //                                {
    //                                    done = true;                                         
    //                                    break;
    //                                }
    //                            }                                 
    //                            if (done) break;                             
    //                        }                             
    //                        // Add quad
    //                        bool flip = false;                             
    //                        x[u] = i;                            
    //                        x[v] = j;                             
    //                        int[] du = new int[3];                             
    //                        int[] dv = new int[3];                             
    //                        if (c > -1)
    //                        {
    //                            du[u] = w;
    //                            dv[v] = h;
    //                        }
    //                        else
    //                        {
    //                            flip = true;
    //                            c = -c;
    //                            du[u] = w;
    //                            dv[v] = h;
    //                        }
 
 
    //                        Vector3 v1 = new Vector3(x[0], x[1], x[2]);
    //                        Vector3 v2 = new Vector3(x[0] + du[0], x[1] + du[1], x[2] + du[2]);
    //                        Vector3 v3 = new Vector3(x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2]);
    //                        Vector3 v4 = new Vector3(x[0] + dv[0], x[1] + dv[1], x[2] + dv[2]);
 
    //                        if (c > 0 && !flip)
    //                        {
    //                            AddFace(v1, v2, v3, v4, vertices, elements, 0);
    //                        }
 
    //                        if (flip)
    //                        {
    //                            AddFace(v4, v3, v2, v1, vertices, elements, 0);
    //                        }
 
    //                        // Zero-out mask
    //                        for (l = 0; l < h; ++l)
    //                            for (k = 0; k < w; ++k)
    //                            {
    //                                mask[n + k + l * size] = 0;
    //                            }
 
    //                        // Increment counters and continue
    //                        i += w; n += w;
    //                    }
 
    //                    else
    //                    {
    //                        ++i;
    //                        ++n;
    //                    }
    //                }
    //            }
    //        }
    //    }
 
    //    Mesh mesh = new Mesh();
    //    mesh.Clear();
    //    mesh.vertices = vertices.ToArray();
    //    mesh.triangles = elements.ToArray();
    //    mesh.RecalculateBounds();
    //    mesh.RecalculateNormals();
    //    return mesh;
    //}

    void BuildFaces(byte map, uint light, Vector3 corner, Vector3 up, Vector3 right, bool reverse, List<Vector3> verts, List<Vector2> uvs, List<int> tris, List<Color32> colors, Direction dir)
    {
        int index = verts.Count;

        verts.Add(corner);
        verts.Add(corner + up);
        verts.Add(corner + up + right);
        verts.Add(corner + right);
        Color32 col = new Color32((byte)(light >> 24), (byte)(light >> 16), (byte)(light >> 8), (byte)(light >> 0));

        switch (dir)
        {
            case Direction.Top:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].TopTexture).x, Block.GetMaterial(Blocks[map].TopTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].TopTexture).x, Block.GetMaterial(Blocks[map].TopTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].TopTexture).width, Block.GetMaterial(Blocks[map].TopTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].TopTexture).width, Block.GetMaterial(Blocks[map].TopTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
            case Direction.Bottom:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BottomTexture).x, Block.GetMaterial(Blocks[map].BottomTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BottomTexture).x, Block.GetMaterial(Blocks[map].BottomTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BottomTexture).width, Block.GetMaterial(Blocks[map].BottomTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BottomTexture).width, Block.GetMaterial(Blocks[map].BottomTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
            case Direction.Front:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].FrontTexture).x, Block.GetMaterial(Blocks[map].FrontTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].FrontTexture).x, Block.GetMaterial(Blocks[map].FrontTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].FrontTexture).width, Block.GetMaterial(Blocks[map].FrontTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].FrontTexture).width, Block.GetMaterial(Blocks[map].FrontTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
            case Direction.Back:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BackTexture).x, Block.GetMaterial(Blocks[map].BackTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BackTexture).x, Block.GetMaterial(Blocks[map].BackTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BackTexture).width, Block.GetMaterial(Blocks[map].BackTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].BackTexture).width, Block.GetMaterial(Blocks[map].BackTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
            case Direction.Left:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].LeftTexture).x, Block.GetMaterial(Blocks[map].LeftTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].LeftTexture).x, Block.GetMaterial(Blocks[map].LeftTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].LeftTexture).width, Block.GetMaterial(Blocks[map].LeftTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].LeftTexture).width, Block.GetMaterial(Blocks[map].LeftTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
            case Direction.Right:
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].RightTexture).x, Block.GetMaterial(Blocks[map].RightTexture).y));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].RightTexture).x, Block.GetMaterial(Blocks[map].RightTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].RightTexture).width, Block.GetMaterial(Blocks[map].RightTexture).height));
                uvs.Add(new Vector2(Block.GetMaterial(Blocks[map].RightTexture).width, Block.GetMaterial(Blocks[map].RightTexture).y));
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                colors.Add(col);
                break;
        }



        if (reverse)
        {
            tris.Add(index + 0);
            tris.Add(index + 1);
            tris.Add(index + 2);
            tris.Add(index + 2);
            tris.Add(index + 3);
            tris.Add(index + 0);
        }
        else
        {
            tris.Add(index + 1);
            tris.Add(index + 0);
            tris.Add(index + 2);
            tris.Add(index + 3);
            tris.Add(index + 2);
            tris.Add(index + 0);
        }
    }

    bool IsVissible(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0 || x >= World.ChunkSize || y >= World.ChunkHeight || z >= World.ChunkSize)
        {
            return true;
        }
        else if (Map[x, y, z] == 0)
        {
            return true;
        }
        return false;
    }

    public bool SetBlock(byte block, Vector3 pos)
    {
        pos -= transform.position;

        return SetBlock(block, Mathf.CeilToInt(pos.x) - 1, Mathf.CeilToInt(pos.y) - 1, Mathf.CeilToInt(pos.z) - 1);
    }

    public bool SetBlock(byte block, int x, int y, int z)
    {
        if ((x < 0) || (y < 0) || (z < 0) || (x >= World.ChunkSize) || (y >= World.ChunkHeight) || (z >= World.ChunkSize))
        {
            if (x < 0)
            {
                return World.FindChunk((int)((this.transform.position.x - World.ChunkOffset) / 16.0f) - 1, (int)((this.transform.position.z - World.ChunkOffset) / 16.0f)).GetComponent<Chunk>().SetBlock(block, 16 + x, y, z);
            }
            else if (y < 0)
            {
                //TODO: Change if Y have more than one chunk
                return false;
            }
            else if (z < 0)
            {
                return World.FindChunk((int)((this.transform.position.x - World.ChunkOffset) / 16.0f), (int)((this.transform.position.z - World.ChunkOffset) / 16.0f) - 1).GetComponent<Chunk>().SetBlock(block, x, y, 16 + z);
            }
            else if (x >= World.ChunkSize)
            {
                return World.FindChunk((int)((this.transform.position.x - World.ChunkOffset) / 16.0f) + 1, (int)((this.transform.position.z - World.ChunkOffset) / 16.0f)).GetComponent<Chunk>().SetBlock(block, x - 16, y, z);
            }
            else if (y >= World.ChunkHeight)
            {
                //TODO: Change if Y have more than one chunk
                return false;
            }
            else if (z >= World.ChunkSize)
            {
                return World.FindChunk((int)((this.transform.position.x - World.ChunkOffset) / 16.0f), (int)((this.transform.position.z - World.ChunkOffset) / 16.0f) + 1).GetComponent<Chunk>().SetBlock(block, x, y, z - 16);
            }
            else
            {
                return false;
            }
        }



        Map[x, y, z] = block;

        StartCoroutine(CreateMesh());

        return true;
    }

    //public static void AddChunk(Chunk2 chunk)
    //{
    //    lock (ChunkQueue)
    //    {
    //        ChunkQueue.Enqueue(chunk);
    //        ChunkQueue.OrderBy(c => Vector3.Distance(c.transform.position, Camera.main.transform.position));
    //    }
    //}
    private Color32[] c;
    IEnumerator setlights()
    {
        int l = Mesh.colors32.Length;
        if (c == null)
             c = new Color32[l];
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        //Mesh = new Mesh();
        for (int i = 0; i < l; i++)
        {
            c[i] = new Color32(255, 255, 255, 255);
        }
        Debug.Log(sw.Elapsed);
        Mesh.colors32 = c;
        Debug.Log(sw.Elapsed);
        yield return new WaitForEndOfFrame();
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 30), "Colors!"))
        {
            StartCoroutine(setlights());
            //Mesh.RecalculateBounds();
            //Mesh.RecalculateNormals();

            //this.GetComponent<MeshFilter>().mesh = Mesh;

            //this.GetComponent<MeshCollider>().sharedMesh = Mesh;
        }
    }
}
