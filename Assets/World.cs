﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    public static World CurrentWorld { get; set; }
    public static Material SpriteSheet;

    public const int ChunkSize = 16;
    public const int ChunkHeight = 64;
    public const float ChunkOffset = 0.5f;
    //[SerializeField]
    //public Material mat;
    public int ViewDistance { get; set; }
    public int Seed { get; set; }

    public List<Chunk> Chunks { get; set; }

    // Use this for initialization
    void Awake()
    {
        CurrentWorld = this;
        SpriteSheet = new Material(Shader.Find("RGB Vertex Lit"));
        SpriteSheet.SetTexture("_MainTex", Resources.Load<Texture>("SpriteSheet"));
        ViewDistance = 64;
        Chunks = new List<Chunk>();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < Chunks.Count; i++)
        {
            Vector3 pos = Chunks[i].transform.position + new Vector3(ChunkSize / 2, 0, ChunkSize / 2);
            Vector3 delta = pos - Camera.main.transform.position;
            delta.y = 0;
            if (delta.magnitude < ViewDistance + (ChunkSize * 3))
                continue;

            Destroy(Chunks[i].gameObject);
            Chunks.RemoveAt(i);
        }

        for (float x = Camera.main.transform.position.x - ViewDistance; x < Camera.main.transform.position.x + ViewDistance; x += ChunkSize)
        {
            for (float z = Camera.main.transform.position.z - ViewDistance; z < Camera.main.transform.position.z + ViewDistance; z += ChunkSize)
            {
                Vector3 pos = new Vector3(Mathf.RoundToInt(x / (float)ChunkSize) * ChunkSize, 0, Mathf.RoundToInt(z / (float)ChunkSize) * ChunkSize);

                Vector3 delta = pos - Camera.main.transform.position;
                delta.y = 0;

                if (delta.magnitude > ViewDistance)
                    continue;

                if (FindChunk((int)(pos.x / ChunkSize), (int)(pos.z / ChunkSize)) != null)
                    continue;

                GameObject go = new GameObject();
                go.name = "Chunk[" + (int)(pos.x / World.ChunkSize) + ";" + (int)(pos.z / World.ChunkSize) + "]";
                go.transform.position = new Vector3((int)pos.x + ChunkOffset, 0, (int)pos.z + ChunkOffset);
                Chunks.Add(go.AddComponent<Chunk>());
            }
        }
    }

    public static GameObject FindChunk(int x, int z)
    {
        //return World.CurrentWorld.Chunks.First(c => c.gameObject.name == "Chunk[" + x + ";" + z + "]").gameObject;
        return GameObject.Find("Chunk[" + x + ";" + z + "]");
    }
}
