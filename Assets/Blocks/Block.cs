﻿using System;
using UnityEngine;

public class Block
{
    public int ID { get; private set; }

    public int TopTexture { get; private set; }
    public int BottomTexture { get; private set; }
    public int SideTexture { get; private set; }

    private int _LeftTexture;
    private int _RightTexture;
    private int _FrontTexture;
    private int _BackTexture;
    public int LeftTexture
    {
        get
        {
            if (_LeftTexture == 0)
            {
                return SideTexture;
            }
            else
            {
                return _LeftTexture;
            }
        }
        private set
        {
            _LeftTexture = value;
        }
    }
    public int RightTexture
    {
        get
        {
            if (_RightTexture == 0)
            {
                return SideTexture;
            }
            else
            {
                return _RightTexture;
            }
        }
        private set
        {
            _RightTexture = value;
        }
    }
    public int FrontTexture
    {
        get
        {
            if (_FrontTexture == 0)
            {
                return SideTexture;
            }
            else
            {
                return _FrontTexture;
            }
        }
        private set
        {
            _FrontTexture = value;
        }
    }
    public int BackTexture
    {
        get
        {
            if (_BackTexture == 0)
            {
                return SideTexture;
            }
            else
            {
                return _BackTexture;
            }
        }
        private set
        {
            _BackTexture = value;
        }
    }

    public Block(int id, int toptexture, int sidetexture, int bottomtexture)
    {
        ID = id;
        TopTexture = toptexture;
        SideTexture = sidetexture;
        BottomTexture = bottomtexture;
    }

    public static Rect GetMaterial(int no)
    {
        return new Rect((((float)no - 1.0f) % 16.0f) / 16.0f, 1.0f - ((int)(((float)no + 16.0f) / 16.0f)) / 16.0f, (((float)no % 16.0f) / 16.0f), 1.0f - ((int)((float)no / 16.0f)) / 16.0f);
    }
}

enum Direction
{
    Top, Bottom, Left, Right, Front, Back,
}